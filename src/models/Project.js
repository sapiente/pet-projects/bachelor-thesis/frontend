import {createModel} from "../utils/functions";

const Project = createModel("Project", (json) => ({
    id: json.id,
    userId: json.userId,
    name: json.name,
}));

export default Project;