import {createModel} from "../utils/functions";

const User = createModel('User', (json) => ({
    id: json.id,
    username: json.username,
}));

export default User;