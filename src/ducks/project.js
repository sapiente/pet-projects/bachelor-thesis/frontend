// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {List, Map} from "immutable/dist/immutable";
import Project from "../models/Project";
import {doDelete, doGet, doPatch, doPost} from "../core/fetch";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/projects`;

const ADD_PROJECT = `${ACTION_PREFIX}/ADD_PROJECT`;
const FETCH_PROJECTS = `${ACTION_PREFIX}/FETCH_PROJECTS`;
const SELECT_PROJECT = `${ACTION_PREFIX}/SELECT_PROJECT`;
export const DELETE_PROJECT = `${ACTION_PREFIX}/DELETE_PROJECT`;
const RENAME_PROJECT = `${ACTION_PREFIX}/RENAME_PROJECT`;


const defaultState = Map({
    selected: null,
    all: List(),
});

// Reducer
export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case ADD_PROJECT: return state.update('all', (all) => all.push(Project(action.payload)));
        case FETCH_PROJECTS: return state.set('all', action.payload);
        case DELETE_PROJECT: return state.update('all', (all) => all.remove(findProjectIndexById(state, action.payload))).set('selected', null);
        case RENAME_PROJECT: return state.setIn(['all', findProjectIndexById(state, action.payload.id), 'name'], action.payload.name);
        case SELECT_PROJECT: return state.set('selected', action.payload);
        default: return state;
    }
}

function findProjectIndexById(projects, id) {
    return projects.get('all').findIndex(project => project.id === id);
}

// Action Creators
export function fetchAll() {
    return (dispatch) => doGet("/projects")
        .then(({data}) => dispatch({ type: FETCH_PROJECTS, payload: Project.fromServerList(data)}));
}

export function addProject(name) {
    return (dispatch) => doPost("/projects", {name})
        .then(({data}) => dispatch({ type: ADD_PROJECT, payload: Project.fromServer(data)}));
}

export function deleteProject(id) {
    return (dispatch) => doDelete(`/projects/${id}`)
        .then(() => dispatch({ type: DELETE_PROJECT, payload: id }));
}

export function renameProject(id, name) {
    return (dispatch) => doPatch(`/projects/${id}`, {name})
        .then(() => dispatch({ type: RENAME_PROJECT, payload: {id, name} }));
}

export function selectProject(projectId) {
    return { type: SELECT_PROJECT, payload: projectId };
}

// Selectors
export const getProject = (state, id) => state.project.get('all').find(project => project.id === id);
export const getSelectedProjectId = (state) => state.project.get('selected');
export const getProjects = (state) => state.project.get('all');
