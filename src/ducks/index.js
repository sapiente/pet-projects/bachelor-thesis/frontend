import {combineReducers} from "redux";

import user from './user'
import todo from './todo'
import project from "./project";

export default combineReducers({user, todo, project});