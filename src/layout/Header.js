import React from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {getLoggedUser, logout} from "../ducks/user";
import User from "../models/User";
import {isCalendarFilter, setSearchFilter} from "../ducks/todo";

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {search: ''};
        this.setState = this.setState.bind(this);
    }

    setSearch(search) {
        this.setState({search: search});
        this.props.setSearchFilter(search);
    }

    static getDerivedStateFromProps(props) {
        return props.isCalendarFilter ? {search: ''} : null;
    }

    render() {
        const {user, logout} = this.props;
        return (<header className="l-header">
            {user ? (
                <React.Fragment>
                    <input value={this.state.search} type="text" className="searchbar" placeholder="Vyhledat" onChange={(e) => this.setSearch(e.target.value)}/>
                    <span>
                <span className="user">{user.username[0]}</span>
                    <i className="fas fa-sign-out-alt" onClick={logout}/>
                </span>
                </React.Fragment>
            ) : null}
        </header>)
    }
}

Header.propTypes = {
    user: PropTypes.instanceOf(User),
    logout: PropTypes.func.isRequired,
    setSearchFilter: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({logout, setSearchFilter}, dispatch);

const mapStateToProps = (state) => ({user: getLoggedUser(state), isCalendarFilter: isCalendarFilter(state)});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
