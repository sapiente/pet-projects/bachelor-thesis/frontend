import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";

import {login, register} from "../../ducks/user";
import Login from "./components/Login";
import Register from "./components/Register";
import {List} from "immutable";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

class LoginContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {errors: List()};

        this.attemptLogin = this.attemptLogin.bind(this);
        this.register = this.register.bind(this);
    }

    attemptLogin(username, password) {
        this.props.login(username, password).catch(() => this.setState({errors: List(['Nepodařilo se přihlásit, zkuste to prosím znovu.'])}));
    }

    register(username, password, callback) {
        this.props.register(username, password)
            .then(() => this.setState({errors: List()}))
            .then(callback)
            .catch(() => this.setState({errors: List(["Nepodařilo se registrovat uživatele."])}));
    }

    render() {
        return (
            <Tabs>
                <TabList>
                    <Tab>Přihlášení</Tab>
                    <Tab>Registrace</Tab>
                </TabList>

                <TabPanel>
                    <Login attemptLogin={this.attemptLogin} errors={this.state.errors}/>
                </TabPanel>
                <TabPanel>
                    <Register register={this.register} errors={this.state.errors} />
                </TabPanel>
            </Tabs>
        );
    }
}

LoginContainer.propTypes = {
    register: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
    register, login
}, dispatch);

export default connect(null, mapDispatchToProps)(LoginContainer);