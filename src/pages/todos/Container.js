import React from 'react';
import LeftSidePanel from "./components/leftSidePanel/Container";
import RightSidePanel from "./components/rightSidePanel/Container";

const TodosContainer = () => (
    <React.Fragment>
        <LeftSidePanel />
        <RightSidePanel />
    </React.Fragment>
);

export default TodosContainer;