import React, {Component} from 'react';
import * as ImmutablePropTypes from "react-immutable-proptypes";
import Todo from "./Todo";
import connect from "react-redux/es/connect/connect";
import {getVisibleTodos, fetchAll as fetchAllTodos} from "../../../../ducks/todo";
import AddTodo from "./AddTodo";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";

class Container extends Component {

    componentDidMount() {
        this.props.fetchAllTodos();
    }

    render() {
        let {todos} = this.props;
        return (
            <div className='l-main-right-panel'>
                {todos.map((todo, index) => <Todo key={index} todo={todo}/>)}
                <AddTodo/>
            </div>
        );
    }
}

Container.protoTypes = {
    todos: ImmutablePropTypes.listOf(PropTypes.instanceOf(Todo)).isRequired,
    fetchAllTodos: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    todos: getVisibleTodos(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({fetchAllTodos}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Container);