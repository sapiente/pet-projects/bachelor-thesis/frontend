import React, {Component} from 'react';
import PropType from 'prop-types';
import {bindActionCreators} from "redux";
import {deleteTodo, editTodo} from "../../../../ducks/todo";
import TodoModel from '../../../../models/Todo'
import Project from "../../../../models/Project";
import {connect} from "react-redux";

import 'react-datez/dist/css/react-datez.css';
import { ReactDatez } from 'react-datez'
import {getProject} from "../../../../ducks/project";
import TodoForm from "./TodoForm";

class Todo extends Component {

    constructor(props) {
        super(props);

        this.state = {hover: this.props.todo.isDone, inEditMode: false};
        this.toggleHover = this.toggleHover.bind(this);
        this.editTodo = this.editTodo.bind(this);
        this.doneTodo = this.doneTodo.bind(this);
        this.scheduleTodo = this.scheduleTodo.bind(this);
    }

    toggleHover() {
        this.setState((state) => ({hover: !state.hover}));
    }

    editTodo(name, priority, projectId, dueDate) {
        const {todo} = this.props;
        this.props.editTodo(todo.id, name, priority, projectId, todo.isDone, dueDate);
        this.setState({inEditMode: false});
    }

    doneTodo(todo) {
        this.props.editTodo(todo.id, todo.name, todo.priority, todo.projectId, !todo.isDone, todo.dueDate).then(this.toggleHover);
    }

    scheduleTodo(todo, date) {
        this.props.editTodo(todo.id, todo.name, todo.priority, todo.projectId, todo.isDone, date);
    }


    render() {
        const {todo, project, deleteTodo} = this.props;

        return this.state.inEditMode ? (
            <TodoForm onCancel={() => this.setState({inEditMode: false})} onSave={this.editTodo} todo={todo}/>
        ) : (
            <div className="todo">
                <span className={`checkbox checkbox-${todo.priority}-priority`} onMouseEnter={this.toggleHover} onMouseLeave={this.toggleHover} onClick={() => this.doneTodo(todo)}>
                    {(this.state.hover)
                        ? (<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M10.041 17l-4.5-4.319 1.395-1.435 3.08 2.937 7.021-7.183 1.422 1.409-8.418 8.591zm-5.041-15c-1.654 0-3 1.346-3 3v14c0 1.654 1.346 3 3 3h14c1.654 0 3-1.346 3-3v-14c0-1.654-1.346-3-3-3h-14zm19 3v14c0 2.761-2.238 5-5 5h-14c-2.762 0-5-2.239-5-5v-14c0-2.761 2.238-5 5-5h14c2.762 0 5 2.239 5 5z"/></svg>)
                        : (<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M5 2c-1.654 0-3 1.346-3 3v14c0 1.654 1.346 3 3 3h14c1.654 0 3-1.346 3-3v-14c0-1.654-1.346-3-3-3h-14zm19 3v14c0 2.761-2.238 5-5 5h-14c-2.762 0-5-2.239-5-5v-14c0-2.761 2.238-5 5-5h14c2.762 0 5 2.239 5 5z"/></svg>)
                    }
                </span>
                <span className="name"> {todo.name}&nbsp;</span>
                <span className="name"> {(project && project.name) || 'Inbox'}&nbsp;</span>
                <ReactDatez
                    name={`date-${todo.id}`}
                    handleChange={(dueDate) => this.scheduleTodo(todo, dueDate)}
                    value={todo.dueDate || ''}
                    locale={'cs'}
                />
                <span className='actions'>
                    <i className="action fas fa-edit" onClick={() => this.setState({inEditMode: true})} />
                    <i className="action fas fa-trash-alt" onClick={() => deleteTodo(todo.id)} />
                </span>
            </div>
        );
    }
}

Todo.propTypes = {
    todo: PropType.instanceOf(TodoModel).isRequired,
    project: PropType.instanceOf(Project),
    deleteTodo: PropType.func.isRequired,
    editTodo: PropType.func.isRequired,
};

const mapStateToProps = (state, {todo}) => ({
    project: getProject(state, todo.projectId)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({deleteTodo, editTodo}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Todo);