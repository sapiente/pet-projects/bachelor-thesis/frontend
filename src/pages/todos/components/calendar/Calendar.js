import React, {Component} from 'react';
import moment from "moment";
import {CALENDAR_VIEW_DAY, CALENDAR_VIEW_MONTH, CALENDAR_VIEW_WEEK} from "../../../../utils/constants";
import {connect} from "react-redux";
import {isCalendarFilter, resetFilter, setCalendarFilter} from "../../../../ducks/todo";
import {bindActionCreators} from "redux";

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: moment(),
            selectedDate: moment(),
            view: CALENDAR_VIEW_DAY,
        };

        this.changeDate = this.changeDate.bind(this);
        this.renderHeader = this.renderHeader.bind(this);
        this.changeView = this.changeView.bind(this);
    };

    changeView(view) {
        this.setState({view: view});
        this.props.setCalendarFilter(this.state.selectedDate, view);
    }

    changeDate(amount) {
        this.setState(state => ({date: state.date.clone().add(amount, 'month')}));
    }

    onDateClick = day => {
        this.setState({selectedDate: day});
        this.props.setCalendarFilter(day, this.state.view);
    };

    renderHeader() {
        const dateFormat = "MMMM YYYY";

        return (
            <div className="header row flex-middle">
                <div className="col col-start">
                    <div className="icon" onClick={() => this.changeDate(-1)}>
                        chevron_left
                    </div>
                </div>
                <div className="col col-center">
                    <span>
                        {this.state.date.format(dateFormat)}
                    </span>
                </div>
                <div className="col col-end">
                    <div className="icon" onClick={() => this.changeDate(1)}>
                        chevron_right
                    </div>
                </div>
            </div>
        );
    }

    renderCells() {
        const {date, selectedDate} = this.state;
        const firstDayMonth = date.clone().startOf('month');
        const firstDayWeek = date.clone().startOf('week');

        const day = firstDayMonth.clone().startOf('week');
        const endDate = firstDayMonth.clone().endOf('month').endOf('week');

        const rows = [];

        rows.push((
            <div className="row" key="days">
                {[0, ...Array(6).fill(1)].map((day, index) => (
                    <div className="col cell day-name" key={index}>
                        {firstDayWeek.add(day,"days").format("ddd")}
                    </div>
                ))}
            </div>
        ));

        while (day <= endDate) {
            rows.push(
                <div className="row" key={day}>
                    {[...Array(7)].map(() => {
                        const cloneDay = day.clone();
                        const isSelected = this.props.isCalendarFilter && cloneDay.isSame(selectedDate, this.state.view);
                        const isDisabled = !cloneDay.isSame(firstDayMonth, 'month');
                        const cursor =  cloneDay.isSame(moment(), 'day');

                        day.add(1, 'days');

                        return (
                            <div className={`col cell ${isSelected && "selected"} ${isDisabled && "disabled"} ${cursor && 'cursor'}`}
                                 key={cloneDay}
                                 onClick={() => this.onDateClick(cloneDay)}
                            >
                                <div className="number">{cloneDay.get('date')}</div>
                            </div>
                        );
                    })}
                </div>
            );
        }
        return <div className="body">{rows}</div>;
    }

    render() {
        return (
            <div className="calendar">
                <div className="area">
                    {this.renderHeader()}
                    {this.renderCells()}
                </div>
                <div className="buttons">
                    <div className={`btn btn-primary ${this.state.view === CALENDAR_VIEW_DAY && 'selected'}`} onClick={() => this.changeView(CALENDAR_VIEW_DAY)}>Den</div>
                    <div className={`btn btn-primary ${this.state.view === CALENDAR_VIEW_WEEK && 'selected'}`} onClick={() => this.changeView(CALENDAR_VIEW_WEEK)}>Týden</div>
                    <div className={`btn btn-primary ${this.state.view === CALENDAR_VIEW_MONTH && 'selected'}`} onClick={() => this.changeView(CALENDAR_VIEW_MONTH)}>Měsíc</div>
                    {this.props.isCalendarFilter && <i className="action fas fa-redo" onClick={this.props.resetFilter}/>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ isCalendarFilter: isCalendarFilter(state) });
const mapDispatchToProps = (dispatch) => bindActionCreators({ setCalendarFilter, resetFilter }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);