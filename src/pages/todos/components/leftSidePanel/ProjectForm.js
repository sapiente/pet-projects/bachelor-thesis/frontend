import React, {Component} from 'react';
import PropTypes from "prop-types";
import Project from "../../../../models/Project";

class ProjectForm extends Component {
    constructor(props) {
        super(props);
        const project = this.props.project || {};

        this.state = {name: project.name || ''};
    }

    render() {
        return (
            <React.Fragment>
                <input value={this.state.name} onChange={(e) => this.setState({name: e.target.value})} />
                <div style={{marginTop: '15px'}}>
                    <div style={{margin: 0}} className="btn btn-primary" onClick={() => this.props.onSave(this.state.name)}>Uložit</div>
                    <div className="btn btn-secondary" onClick={this.props.onCancel}>Zrušit</div>
                </div>
            </React.Fragment>
        );
    }
}

ProjectForm.protoTypes = {
    project : PropTypes.instanceOf(Project),
    onSave : PropTypes.func.isRequired,
    onCancel : PropTypes.func.isRequired,
};

export default ProjectForm;