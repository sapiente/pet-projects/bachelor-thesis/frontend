import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as ImmutablePropTypes from "react-immutable-proptypes";
import {connect} from "react-redux";
import Project from "./Project";
import AddProject from "./AddProject";
import {getProjects, getSelectedProjectId, selectProject, fetchAll as fetchAllProjects} from "../../../../ducks/project";
import {bindActionCreators} from "redux";
import Calendar from "../calendar/Calendar";

class Container extends Component {

    componentDidMount() {
        this.props.fetchAllProjects();
    }

    render() {
        let {projects, selectedProjectId, selectProject} = this.props;
        return (
            <div className='l-main-left-panel'>
                <Calendar/>
                <div className={`link project ${(selectedProjectId === 0) && 'selected'}`} onClick={() => {
                    selectProject(0)
                }}>
                    <i className="primary fas fa-inbox"/>Inbox
                </div>
                <div className={`link project ${(selectedProjectId === null) && 'selected'}`} onClick={() => {
                    selectProject(null)
                }}>
                    <i className="primary fas fa-list-ul"/>Vše
                </div>
                {projects.map((project) => <Project key={project.id} selectedProjectId={selectedProjectId}
                                                    project={project}/>)}
                <AddProject/>
            </div>
        );
    }
}

Container.protoTypes = {
    projects: ImmutablePropTypes.listOf(PropTypes.instanceOf(Project)).isRequired,
    selectedProjectId: PropTypes.number,
    selectProject: PropTypes.func.isRequired,
    fetchAllProjects: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({selectProject, fetchAllProjects}, dispatch);

const mapStateToProps = (state) => ({
    projects: getProjects(state),
    selectedProjectId: getSelectedProjectId(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);