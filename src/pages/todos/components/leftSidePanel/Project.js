import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteProject, renameProject, selectProject} from "../../../../ducks/project";
import ProjectForm from "./ProjectForm";

class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {inEditMode: false};

        this.renameProject = this.renameProject.bind(this);
    }

    renameProject(name) {
        this.props.renameProject(this.props.project.id, name);
        this.setState({inEditMode: false});
    }

    render() {
        const {project, selectedProjectId, deleteProject, selectProject} = this.props;

        return this.state.inEditMode ? (
                <ProjectForm project={project} onSave={this.renameProject} onCancel={() => this.setState({inEditMode: false})}/>
            ) : (
                <div className={`link project ${(project.id === selectedProjectId) && 'selected'}`} onClick={() => {selectProject(project.id)}}>
                    <i className="primary fas fa-list-ul"/>
                    {project.name}
                    <span className='actions'>
                        <i className="action fas fa-edit" onClick={() => this.setState({inEditMode: true})} />
                        <i className="action fas fa-trash-alt" onClick={(e) => {deleteProject(project.id); e.stopPropagation();}} />
                    </span>
                </div>
            );
    }
}

Project.protoTypes = {
    project: PropTypes.instanceOf(Project).isRequired,
    selectedProjectId: PropTypes.number.isRequired,
    deleteProject: PropTypes.func.isRequired,
    renameProject: PropTypes.func.isRequired,
    selectProject: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => bindActionCreators({deleteProject, renameProject, selectProject}, dispatch);

export default connect(null, mapDispatchToProps)(Project);