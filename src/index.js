import React from 'react';
import {render} from 'react-dom';
import App from './layout/App';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from "redux";
import modules from './ducks';

import thunk from 'redux-thunk'
import {setAuthToken, setBaseUrl} from "./core/fetch";
import {getToken} from "./core/auth";
import {fetchIdentity} from "./ducks/user";

const enhancer = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : (x) => x,
);

const store = createStore(modules, enhancer);


setBaseUrl(process.env.REACT_APP_API_URL);
const token = getToken();
if(token) {
    setAuthToken(token);
    store.dispatch(fetchIdentity(token))
}

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
